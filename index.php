<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $name = $_POST['name'];

    echo "Hello, $name! Welcome to our website";
}
?>

<form method="POST">
    <label for="name">Enter your name:</label>
    <input type="text" name="name" id="name">
    <button type="submit">Submit</button>
</form>